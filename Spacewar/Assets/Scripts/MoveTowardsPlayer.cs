﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsPlayer : MonoBehaviour
{
    private Transform player;

    [Header(" Enemy Properties ")]
    public float speed = 2.0f;
    public int collisionDamage = 2;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("PlayerShip").transform;

    }

    // Update is called once per frame
    void Update()
    {
        // Check the player attribute was set in Start ().
        if (player == null)
        {
            Debug.Log(" Cannot find player ship , please " +
            " check its called \" PlayerShip \"");
        }
        else
        {
            Vector3 vectorToPlayer = player.position - transform.position;

            vectorToPlayer.Normalize();
            float dist = speed * Time.deltaTime;
            Vector3 movement = (vectorToPlayer * dist);
            transform.position = transform.position + movement;

        }
    }
}
