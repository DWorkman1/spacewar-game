﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public Transform laserPrefab;
    public float laserDistance = 0.2f;
    public float timeBetweenFires = 0.3f;
    private float timeTilNextFire = 0.0f;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1") && timeTilNextFire < 0)
        {
            // Reset the timer and fire .
            timeTilNextFire = timeBetweenFires;
            ShootLaser();
        }
        timeTilNextFire -= Time.deltaTime;
    }
    void ShootLaser()
    {
        Vector3 laserPos = this.transform.position;
        float rotationAngle = transform.localEulerAngles.z - 90;
        float rotAngleRads = (rotationAngle) * Mathf.Deg2Rad;
        laserPos.x += Mathf.Cos(rotAngleRads) * -laserDistance;
        laserPos.y += Mathf.Sin(rotAngleRads) * -laserDistance;
        Instantiate(laserPrefab, laserPos, this.transform.rotation);

    }
}
