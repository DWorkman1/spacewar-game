﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : MonoBehaviour
{
    public int health = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }
            // Update is called once per frame
            void Update()
        {
        }
    void OnCollisionEnter2D(Collision2D theCollision)
    {
        MoveTowardsPlayer enemy = theCollision.gameObject.GetComponent<MoveTowardsPlayer>();
        if (enemy != null)
        {
            health -= enemy.collisionDamage;

            Destroy(theCollision.gameObject);

        }
        else
        {
        }
        if (health <= 0) ;
        {
            Destroy(this.gameObject);
        }
    }
}
