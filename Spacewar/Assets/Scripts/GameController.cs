﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Transform enemy;

    [Header("Wave Properties")]
    public float timeBeforeSpawning = 1.5f;
    public float timeBetweenEnemies = 0.25f;
    public float timeBeforeWaves = 2.0f;

    public int enemiesPerWave = 10;

    private int currentNumberOfEnemies = 0;
    
    private void createOneRandomEnemy()
    {
        float randDistance = Random.Range(10, 25);
        Vector2 randDirection = Random.insideUnitCircle;
        Vector3 enemyPos = this.transform.position;
        enemyPos.x += randDirection.x * randDistance;
        enemyPos.y += randDirection.y * randDistance;
        Instantiate(enemy, enemyPos, this.transform.rotation);

    }
    // Start is called before the first frame update
    void Start()
    {
        createOneRandomEnemy();
    }

    IEnumerator spawnEnemies()
    {
        yield return new WaitForSeconds(timeBeforeSpawning);
        while (true)
        {
            if (currentNumberOfEnemies <= 0)
            {
                for (int i = 0; i < enemiesPerWave; i++)
                {
                    createOneRandomEnemy();
                    currentNumberOfEnemies++;

                    yield return new WaitForSeconds(timeBetweenEnemies);
                }
            }
            yield return new WaitForSeconds(timeBeforeWaves);
        }

            // Update is called once per frame
            void Update()
    {
        
    }
}
