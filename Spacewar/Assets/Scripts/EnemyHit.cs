﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour
{
    public int health = 2;
    public int pointsPerEnemy = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter2D(Collision2D theCollision)
    {
        LaserMovement laser = theCollision.gameObject.GetComponent<LaserMovement>();
        if (laser != null)
        {
            health -= laser.damage;
            Destroy(theCollision.gameObject);
        }
        else
        {
            /* We ’ve hit another ship */
        }
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    }
